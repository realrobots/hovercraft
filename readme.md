# RealRobots.net Hovercraft

### Introduction

This is the first prototype version of a hovercraft that students my students will be making.

The goal is to provide basic parts like motors, controllers, propellers and maybe some more complicated parts like steering assemblies, but to have students assemble them and build the chassis themselves, to their own design as much as possible.

The hovercraft works by having a flat base and pushing air underneath it to separate the bottom surface from the ground and reduce friction. A plastic bag is added to be inflated to allow a larger cushion that can travel over small bumps and obstacles.

I'll be continuing to improve the design before assigning it as a project to STEM students to build. 

Support on [Patreon](https://www.patreon.com/realrobots) is greatly appreciated.

### Test

Electronics Test
https://www.youtube.com/embed/-F3Q4BDdYuk

First Test Drive
https://www.youtube.com/watch?v=Wk8S0R-ZeuE


### Latest Changes

Smaller hover motor prop, narrower and higher ducting.
![](IMG_20211018_184728.jpg)


Went with large cutout rather than multiple smaller holes, reinforced edge with gaffer tape.
![](IMG_20211018_184715.jpg)


### Design

The base board is foam core board from the art store, the cushion is just black garbage bag thats taped around the outside. For the holes in the bag I first reinforced them with clear tape, then melted holes with a soldering iron.

The motor mounts and steering assembly are 3d printed PLA.



Foam core board with garbage bag taped on and hover motor mounted.

![](IMG_20211017_183641.jpg)

The controller board is just and Arduino Nano that communicates with the transmitter through an nrf24l01 board and with 3 connections for the steering servo, hover motor and thrust motor. 

![](IMG_20211017_205443.jpg)

11.1v battery provides power for the craft. I tried it with a 7.4v pack of 2 x 18650 cells first but it was a bit anemic, it could get off the ground and thrust but only barely at max power.  Code is NFReceiver.

![](IMG_20211017_205453.jpg)

The transmitter is another Arduino Nano and nrf24l01  combination, with a potentiometer and thumbstick to provide inputs. Code is NFTransmitter. Single 18650 3.7v cell used for power.

![](IMG_20211017_205518.jpg)

![](IMG_20211017_205521.jpg)

Motor and ESC were just literally the cheapest ones I could find. It's an A2212 1400KV motor with a 30A ESC that I have no other info about.

![](IMG_20211017_205530.jpg)