//Include Libraries
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define PIN_HOVER A2
#define PIN_THRUST A0
#define PIN_DIR A1


//create an RF24 object
RF24 radio(9, 8);  // CE, CSN

//address through which two modules communicate.
const byte address[6] = "00004";

struct data{
  int hover;
  int thrust;
  int dir;
 
};
data send_data;

void setup()
{
  Serial.begin(115200);
  Serial.println("TRANSMITTER");
  
  pinMode(PIN_HOVER, INPUT);
  pinMode(PIN_THRUST, INPUT);
  pinMode(PIN_DIR, INPUT);
  
  if (!radio.begin()) {
    Serial.println(F("radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }
  
  //set the address
  radio.openWritingPipe(address);
  
  //Set module as transmitter
  radio.stopListening();
}
void loop()
{
  send_data.hover = analogRead(PIN_HOVER);
  send_data.thrust = analogRead(PIN_THRUST);
  send_data.dir = analogRead(PIN_DIR);

  send_data.hover = map(send_data.hover, 0, 1023, 255, 0);
  //if (send_data.thrust > 520) send_data.thrust = 520;
  send_data.thrust = map(send_data.thrust, 1023, 0, 0, 255);
  send_data.dir = map(send_data.dir, 0, 1023, 255, 0);
  
  //Send message to receiver
  //const char text[] = "Hello World";
  radio.write(&send_data, sizeof(data));
  Serial.print(send_data.hover);
  Serial.print("\t");
  Serial.print(send_data.thrust);
  Serial.print("\t");
  Serial.println(send_data.dir);
  delay(50);
}
