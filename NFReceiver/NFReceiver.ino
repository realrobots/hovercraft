//Include Libraries
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Servo.h>

#define PIN_HOVER 3
#define PIN_THRUST 5
#define PIN_DIR 6

Servo thrust;
Servo dir;

//create an RF24 object
RF24 radio(9, 8);  // CE, CSN

//address through which two modules communicate.
const byte address[6] = "00004";

long lastReceive = 1000;

struct data {
  int hover;
  int thrust;
  int dir;

};
data send_data;

void setup()
{

  Serial.begin(115200);
  
  pinMode(PIN_HOVER, OUTPUT);
  thrust.attach(PIN_THRUST);
  dir.attach(PIN_DIR);

  if (!radio.begin()) {
    Serial.println(F("radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }
  Serial.println("Reciever Active");

  //set the address
  radio.openReadingPipe(0, address);

  //Set module as receiver
  radio.startListening();
}

void loop()
{
  //Read the data if available in buffer
  if (radio.available())
  {
    char text[32] = {0};
    radio.read(&send_data, sizeof(data));
    //send_data.thrust = map(send_data.thrust, 20, 140, 0, 255);
    Serial.print(send_data.hover);
    Serial.print("\t");
    Serial.print(send_data.thrust);
    Serial.print("\t");
    Serial.print(send_data.dir);

    if (send_data.thrust < 129) {
      send_data.thrust = 129;
    }
    send_data.thrust = map(send_data.thrust, 129, 255, 20, 140);
    Serial.print("\t");
    Serial.print(send_data.thrust);

    send_data.dir = map(send_data.dir, 0, 255, 20, 140);

    Serial.print("\t");
    Serial.println(send_data.dir);

    analogWrite(PIN_HOVER, send_data.hover);
    thrust.write(send_data.thrust);
    dir.write(send_data.dir);

    lastReceive = millis();
  }

  if (millis() - lastReceive > 1000) {
    EStop();
  }

  delay(20);
}

void EStop() {
  send_data.hover = 0;
  send_data.thrust = 22;
  send_data.dir = 90;
  analogWrite(PIN_HOVER, send_data.hover);
  //hover.write(send_data.hover);
  thrust.write(send_data.thrust);
  dir.write(send_data.dir);
}
